﻿using UnityEngine;
using System.Collections;

public class PlayerDomino : MonoBehaviour {

	int player = 0;							// player type
	bool canSelect = false;			// can domino be selected
						
	public int no1;							// domino numbers
	public int no2;							//
	bool cpu = false;						// is this domino for cpu

	Vector3 newPos;						// new position
	Vector3 cPos;							// current position
	bool moving = false;				// is domino moving to new position
	float timer = 0.0f;						// count time
	
	Game gscript;							//

	// 
	void Awake () {
		gscript = GameObject.Find("Game").GetComponent<Game>();
	}
	
	// 
	void Update () {
		// test if this domino is cpu domino
		if(!cpu){
			if(!moving){
				if(gscript.playerTurn == Game.PlayerTurn.Player1){
					// can domino be selected
					TestCanSelect();

					if(canSelect){
						if(gscript.selected == gameObject){
							// set domino color
							gameObject.GetComponent<Renderer>().material.color = new Color(0.0f, 1.0f, 0.0f);
						}else{
							gameObject.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
						}
					}else{
						gameObject.GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 0.6f);
					}
				}else{
					gameObject.GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 0.6f);
				}
			}else{
				gameObject.GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 0.6f);
			}
		}

		if(cpu){
			// test if domino is part of players on turn stack
			if(player == (int)gscript.playerTurn+1){
				gameObject.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
			}else{
				gameObject.GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 0.6f);
			}
		}

		if(moving){
			if(timer < 1.0f){
				timer += 5.0f*Time.deltaTime;
				if(timer > 1.0f){
					timer = 1.0f;
				}
				// move domino to new position
				transform.position = Vector3.Lerp(cPos, newPos, timer);
			}else{
				timer = 0.0f;
				transform.position = newPos;
				moving = false;
			}
		}
	}

	// create name and asign number values to domino
	public void CreateNameAndNumbers(string a, bool b, int c){
		// set domino name, is part of cpu player, player, create and set domino numbers
		gameObject.name = a;
		cpu = b;
		player = c;
		CreateNumbers();
	}

	// when clicked
	void OnMouseUp(){
		OnFinger();
	}

	// on finger touch
	public void OnFinger(){
		if(!moving && gscript.playerTurn == Game.PlayerTurn.Player1 && !gscript.gameOver){
			if(canSelect){
				if(gscript.selected != gameObject || gscript.selected == null){
					gscript.selected = gameObject;
					PassNumbers();
					gscript.CreatePossibleMoves();
				}
			}
		}
	}

	// on cpu select
	public void OnCPU(){
		// select domino
		gscript.selected = gameObject;
		// set numbers
		PassNumbers();
		// create possible moves for selected domino
		gscript.CreatePossibleMoves();
	}
	
	// create numbers from domino name
	void CreateNumbers(){
		string c1 = gameObject.name[0].ToString();
		no1 = int.Parse(c1);
		string c2 = gameObject.name[1].ToString();
		no2 = int.Parse(c2);
	}

	// set domino numbers
	void PassNumbers(){
		gscript.pNo1 = no1;
		gscript.pNo2 = no2;
	}

	// can domino be selected
	void TestCanSelect(){
		if(gscript.noPlaced < 1){
			canSelect = true;
		}else{
			if(gscript.aNo1 == no1 || gscript.aNo1 == no2 || gscript.aNo2 == no1 || gscript.aNo2 == no2){
				canSelect = true;
			}else{
				canSelect = false;
			}
		}
	}

	// move domino to new position
	public void MoveToPosition(Vector3 newPosition){
		newPos = newPosition;
		cPos = transform.position;
		moving = true;
	}
}
