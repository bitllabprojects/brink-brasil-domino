﻿using UnityEngine;
using System.Collections;

public class NumberPart : MonoBehaviour {

	public int num;											// number
	public bool isDisabled = false;				// is part disabled
}
