﻿using UnityEngine;
using System.Collections;

public class Empty : MonoBehaviour {

	public int toSet;									// number to be set
	GameObject objectToDisable1;			// disable domino checking
	GameObject objectToDisable2;			//
	bool toLeft = false;								// bend side
	bool toRight = false;								// bend side
	Game gscript;										//
	
	// get script
	void Awake(){
		gscript = GameObject.Find("Game").GetComponent<Game>();
	}

	// on mouse click
	void OnMouseUp(){
		if(gscript.playerTurn == Game.PlayerTurn.Player1){
			OnFinger();
		}
	}

	//
	public void GetTouch(){
		if(gscript.playerTurn == Game.PlayerTurn.Player1){
			OnFinger();
		}
	}

	// on touch
	public void OnFinger(){
		GameObject d = Instantiate(gscript.dominoPrefab, transform.position, transform.rotation) as GameObject;

		// if no dominoes placed yet
		if(gscript.noPlaced < 1){
			d.GetComponent<Domino>().SetDomino(gscript.pNo1, gscript.pNo2);
		}else{
			d.GetComponent<Domino>().SetDomino(gscript.pNo1, gscript.pNo2, toSet);
		}

		// add placed dominoes number
		gscript.noPlaced++;

		// if there is object to dissable then dissable it
		if(objectToDisable1 != null){
			objectToDisable1.GetComponent<NumberPart>().isDisabled = true;
		}
		if(objectToDisable2 != null){
			objectToDisable2.GetComponent<NumberPart>().isDisabled = true;
		}
		// bend dominoes row
		d.GetComponent<Domino>().TurnRow(toLeft, toRight);
		// destroy selected domino
		gscript.DestroySelection();
	}

	// domno to be disabled when new domino placed
	public void SetToDisable(GameObject g){
		objectToDisable1 = g;
	}

	// set objects to dissable and set bend side
	public void SetToDisable(GameObject g1, GameObject g2, bool b1, bool b2){
		objectToDisable1 = g1;
		objectToDisable2 = g2;
		toLeft = b1;
		toRight = b2;
	}
}
