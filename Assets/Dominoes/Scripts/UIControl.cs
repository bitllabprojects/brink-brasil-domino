﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIControl : MonoBehaviour {

	public Button newDominoButton;							// draw domino button
	public Button passTurn;											// pass buton
	public Image passImage;										// pass image text
	public Text freeDominoCount;								// number of dominoes left

	public GameObject panel1;									// player play turn indicator
	public GameObject panel2;									//
	public GameObject panel3;									//
	public GameObject panel4;									//

	public GameObject p2ScorePanel;						// score panels
	public GameObject p3ScorePanel;						//
	public GameObject p4ScorePanel;						//

	//
	public Text p1HandP2;											// total dominoes left in player stack(hand)
	public Text p1HandP3;											//
	public Text p1HandP4;											//

	public Text p2HandP2;											//
	public Text p2HandP3;											//
	public Text p2HandP4;											//
	
	public Text p3HandP3;											//
	public Text p3HandP4;											//

	public Text p4HandP4;											//

	//
	public Text p1TotalP2;											// total player score
	public Text p1TotalP3;											//
	public Text p1TotalP4;											//

	public Text p2TotalP2;											//
	public Text p2TotalP3;											//
	public Text p2TotalP4;											//
	
	public Text p3TotalP3;											//
	public Text p3TotalP4;											//
	
	public Text p4TotalP4;											//

	//
	public Button nextRound;										// load next round
	public Button mainMenu;										// load main menu

	bool gameEnd = false;											// game has ended

	bool doScale = false;												// scale image
	float timer = 0.0f;														// count timer

	public GameObject exitPanel;								// exit menu panel

	Game gscript;															//
	Setup setScr;																//


	// Use this for initialization
	void Start(){
		gscript = GameObject.Find("Game").GetComponent<Game>();
		setScr = GameObject.Find("Setup").GetComponent<Setup>();
		// deactivate objects
		passTurn.gameObject.SetActive(false);
		newDominoButton.gameObject.SetActive(false);
		passImage.gameObject.SetActive(false);
	}

	//
	void Update(){
		// scale pass image with timer
		if(doScale){
			if(timer < 1.0f){
				timer += 1.0f*Time.deltaTime;
				passImage.gameObject.SetActive(true);
				passImage.rectTransform.localScale = new Vector3(timer,timer,0);
			}else{
				timer = 0.0f;
				passImage.rectTransform.localScale = new Vector3(timer,timer,0);
				passImage.gameObject.SetActive(false);
				doScale = false;
			}
		}

		// set player play turn indicator
		SetPlayerPanel();
	}

	// check if button
	void LateUpdate(){
		EnableDisableButtons();
	}

	// shpw scores panel
	public void ShowScoresPanel(int w, int h1, int h2, int h3, int h4){
		switch(gscript.playerNo){
		case 2:
			setScr.lastRoundWinner = w;
			ShowP2Scores(w,h1,h2);
			break;
		case 3:
			setScr.lastRoundWinner = w;
			ShowP3Scores(w,h1,h2,h3);
			break;
		case 4:
			setScr.lastRoundWinner = w;
			ShowP4Scores(w,h1,h2,h3,h4);
			break;
		}
	}

	//
	void ShowP2Scores(int w, int h1, int h2){
		p2ScorePanel.SetActive(true);
		p1HandP2.text = h1.ToString();
		p2HandP2.text = h2.ToString();

		SetTotal (w,h1,h2,0,0);

		p1TotalP2.text = setScr.player1Score.ToString();
		p2TotalP2.text = setScr.player2Score.ToString();

		SetTotalColor(w, gscript.playerNo);

		SetButtons();
	}

	//
	void ShowP3Scores(int w, int h1, int h2, int h3){
		p3ScorePanel.SetActive(true);
		p1HandP3.text = h1.ToString();
		p2HandP3.text = h2.ToString();
		p3HandP3.text = h3.ToString();

		SetTotal(w,h1,h2,h3,0);

		p1TotalP3.text = setScr.player1Score.ToString();
		p2TotalP3.text = setScr.player2Score.ToString();
		p3TotalP3.text = setScr.player3Score.ToString();

		SetTotalColor(w, gscript.playerNo);

		SetButtons();
	}

	//
	void ShowP4Scores(int w, int h1, int h2, int h3, int h4){
		p4ScorePanel.SetActive(true);
		p1HandP4.text = h1.ToString();
		p2HandP4.text = h2.ToString();
		p3HandP4.text = h4.ToString();
		p4HandP4.text = h3.ToString();

		SetTotal(w,h1,h2,h3,h4);

		p1TotalP4.text = setScr.player1Score.ToString();
		p2TotalP4.text = setScr.player2Score.ToString();
		p3TotalP4.text = setScr.player4Score.ToString();
		p4TotalP4.text = setScr.player3Score.ToString();

		SetTotalColor(w, gscript.playerNo);

		SetButtons();
	}

	// set winner total score color to green
	void SetTotalColor(int w, int p){
		p1TotalP2.color = new Color(1f,1f,1f);
		p1TotalP3.color = new Color(1f,1f,1f);
		p1TotalP4.color = new Color(1f,1f,1f);

		p2TotalP2.color = new Color(1f,1f,1f);
		p2TotalP3.color = new Color(1f,1f,1f);
		p2TotalP4.color = new Color(1f,1f,1f);

		p3TotalP3.color = new Color(1f,1f,1f);
		p3TotalP4.color = new Color(1f,1f,1f);

		p4TotalP4.color = new Color(1f,1f,1f);

		switch(p){
		case 2:
			switch(w){
			case 1:
				p1TotalP2.color = new Color(0f,1f,0f);
				break;
			case 2:
				p2TotalP2.color = new Color(0f,1f,0f);
				break;
			}
			break;
		case 3:
			switch(w){
			case 1:
				p1TotalP3.color = new Color(0f,1f,0f);
				break;
			case 2:
				p2TotalP3.color = new Color(0f,1f,0f);
				break;
			case 3:
				p3TotalP3.color = new Color(0f,1f,0f);
				break;
			}
			break;
		case 4:
			switch(w){
			case 1:
				p1TotalP4.color = new Color(0f,1f,0f);
				break;
			case 2:
				p2TotalP4.color = new Color(0f,1f,0f);
				break;
			case 3:
				p4TotalP4.color = new Color(0f,1f,0f);
				break;
			case 4:
				p3TotalP4.color = new Color(0f,1f,0f);
				break;
			}
			break;
		}
	}

	// enable and disable buttons
	void SetButtons(){
		if(gameEnd){
			nextRound.gameObject.SetActive(false);
			mainMenu.gameObject.SetActive(true);
		}else{
			nextRound.gameObject.SetActive(true);
			mainMenu.gameObject.SetActive(false);
		}
	}

	// start pass image scale
	public void StartPassScale(){
		doScale = true;
	}

	// set and test total score
	void SetTotal(int w, int h1, int h2, int h3, int h4){
		switch(w){
		case 1:
			setScr.player1Score += h2+h3+h4;
			if(setScr.player1Score >= setScr.targetScore){
				gameEnd = true;
			}
			break;
		case 2:
			setScr.player2Score += h1+h3+h4;
			if(setScr.player2Score >= setScr.targetScore){
				gameEnd = true;
			}
			break;
		case 3:
			setScr.player3Score += h1+h2+h4;
			if(setScr.player3Score >= setScr.targetScore){
				gameEnd = true;
			}
			break;
		case 4:
			setScr.player4Score += h1+h2+h3;
			if(setScr.player4Score >= setScr.targetScore){
				gameEnd = true;
			}
			break;
		}
	}

	// enable or disable draw/pass buttoms
	void EnableDisableButtons(){
		int free = gscript.CountFreeDominoes();
		bool isMoves = GetPossibleMoves();

		if(free > 0 && isMoves == false && gscript.playerTurn == Game.PlayerTurn.Player1 && gscript.noPlaced > 0){
			if(setScr.gameType == "draw"){
				passTurn.gameObject.SetActive(false);
				newDominoButton.gameObject.SetActive(true);
				freeDominoCount.text = free.ToString();
			}else{
				passTurn.gameObject.SetActive(true);
				newDominoButton.gameObject.SetActive(false);
			}
		}else if(free < 1 && gscript.playerTurn == Game.PlayerTurn.Player1 && isMoves == false && gscript.noPlaced > 0){
			newDominoButton.gameObject.SetActive(false);
			passTurn.gameObject.SetActive(true);
		}else{
			passTurn.gameObject.SetActive(false);
			newDominoButton.gameObject.SetActive(false);
		}

		if(gscript.gameOver){
			passTurn.gameObject.SetActive(false);
			newDominoButton.gameObject.SetActive(false);
		}
	}

	// get all possible moves for player
	bool GetPossibleMoves(){
		bool cp = false;
		ArrayList nr = new ArrayList();

		switch(gscript.playerTurn){
		case Game.PlayerTurn.Player1:
			nr = gscript.playerD;
			break;
		case Game.PlayerTurn.Player2:
			nr = gscript.cpu2D;
			break;
		case Game.PlayerTurn.Player3:
			nr = gscript.cpu3D;
			break;
		case Game.PlayerTurn.Player4:
			nr = gscript.cpu4D;
			break;
		}

		// test all dominoes in player stack
		for(int x = 0; x < nr.Count; x++){
			string n = (string)nr[x];
			int n1 = GameObject.Find(n).GetComponent<PlayerDomino>().no1;
			int n2 = GameObject.Find(n).GetComponent<PlayerDomino>().no2;
			if(gscript.aNo1 == n1 || gscript.aNo1 == n2 || gscript.aNo2 == n1 || gscript.aNo2 == n2){
				cp = true;
			}
		}
		
		return cp;
	}

	// set player panel active
	void SetPlayerPanel(){
		if(!gscript.gameOver){
			panel1.SetActive(false);
			panel2.SetActive(false);
			panel3.SetActive(false);
			panel4.SetActive(false);

			switch(gscript.playerTurn){
			case Game.PlayerTurn.Player1:
				panel1.SetActive(true);
				break;
			case Game.PlayerTurn.Player2:
				panel2.SetActive(true);
				break;
			case Game.PlayerTurn.Player3:
				panel3.SetActive(true);
				break;
			case Game.PlayerTurn.Player4:
				panel4.SetActive(true);
				break;
			}
		}else{
			panel1.SetActive(false);
			panel2.SetActive(false);
			panel3.SetActive(false);
			panel4.SetActive(false);
		}
	}

	// show exit menu
	public void ShowExitMenu(){
		exitPanel.SetActive(true);
		Time.timeScale = 0.001f;
	}

	// hide exit menu
	public void HideExitMenu(){
		exitPanel.SetActive(false);
		Time.timeScale = 1.0f;
	}

	// load main menu
	public void LoadMainMenu(){
		setScr.ResetSetup();
		Time.timeScale = 1.0f;
		SceneManager.LoadScene("MainMenu");
	}

	// load game scene
	public void LoadNextRound(){
		SceneManager.LoadScene("Game");
	}
}