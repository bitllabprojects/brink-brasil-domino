﻿using UnityEngine;
using System.Collections;

public class CamPivot : MonoBehaviour {

	public float rotateSpeed;
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(0.0f, rotateSpeed*Time.deltaTime, 0.0f);
	}
}
