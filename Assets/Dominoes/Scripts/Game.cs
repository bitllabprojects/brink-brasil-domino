﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Game : MonoBehaviour {

	public int playerNo;											// number of players
	public int dominoStartNo;								// number of starting tiles
	public int p1Score;											// player 1 score
	public int p2Score;											// player 2 score
	public int p3Score;											// player 3 score
	public int p4Score;											// player 4 score

	public int noPassed = 0;								// passed turns

	public int round;												// game round
	public int noPlaced;										// number of placed dominoes

	public GameObject dominoPrefab;				// domino prefab;
	public GameObject playerDomino;				// player domino that can be clicked
	public GameObject cpuDomino;					// computer domino prefab
	public GameObject emptyDomino;				// empty domino for placing
	public Texture[] dominoTexture;					// domino textures
	public Texture[] playTexture;							// domino play textures

	public int aNo1;												// number 1 to place
	public int aNo2;												// number 2 to place
	public int pNo1;												// selected number 1
	public int pNo2;												// selected number 2

	public string[] allDominoes;							// all domino types
	public bool[] usedDominoes;							// dominoes that are in game

	public Transform P1DH;									// player dominoes group object
	public Transform P2DH;
	public Transform P3DH;
	public Transform P4DH;

	public ArrayList playerD = new ArrayList();					// player dominoes
	public ArrayList cpu2D = new ArrayList();					// cpu dominoes
	public ArrayList cpu3D = new ArrayList();					//
	public ArrayList cpu4D = new ArrayList();					//

	Vector3 p1sp = new Vector3();										// domino creation start position
	Vector3 p2sp = new Vector3();
	Vector3 p3sp = new Vector3();
	Vector3 p4sp = new Vector3();

	public int freeDominoes;											// number of dominoes not in game
	public GameObject selected;										// selected domino

	public enum PlayerTurn{Player1,Player2,Player3,Player4};				// player play turn
	public PlayerTurn playerTurn;																// player to play

	public int toLeft = 0;														// dominoes bend direction
	public int toRight = 0;													//

	public bool showDrawButton;									// show draw button if no moves
	public bool pause;														// game is paused
	
	public bool gameOver;												// is game over

	CPU cpuScr;																	// get scripts
	Setup setScr;																	//
	UIControl uiScr;																//

	// get/set startup variables
	void Start (){
		cpuScr = GameObject.Find("CPU").GetComponent<CPU>();
		setScr = GameObject.Find("Setup").GetComponent<Setup>();
		uiScr = GameObject.Find("CanvasGame").GetComponent<UIControl>();
		playerNo = setScr.playerNo;
		dominoStartNo = setScr.dominoStartNo;
		RoundSetup(dominoStartNo);
	}
	
	// get available numbers
	void Update (){
		GetANumbers();
	}
	
	// set starting board
	void RoundSetup(int dno){
		// get dominoes starting position
		p1sp = P1DH.position;
		p2sp = P2DH.position;
		p3sp = P3DH.position;
		p4sp = P4DH.position;

		// create offset for domino creation
		float sp1 = ((dno-1) * -1.4f)/2;
		p1sp.x += sp1;
		float sp2 = ((dno-1)*-1.4f)/2;
		p2sp.z += sp2;
		float sp3 = ((dno-1)*-1.4f)/2;
		p3sp.z += sp3;
		float sp4 = ((dno-1) * -1.4f)/2;
		p4sp.x += sp4;

		// create new dominoes
		for(int x = 0; x < dno; x++){
			CreatePlayerDomino(p1sp);
		}
	
		if(playerNo == 3 || playerNo == 4){
			for(int x = 0; x < dno; x++){
				CreatePlayer2Domino(p2sp);
			}
		}

		if(playerNo == 3 || playerNo == 4){
			for(int x = 0; x < dno; x++){
				CreatePlayer3Domino(p3sp);
			}
		}

		if(playerNo == 2 || playerNo == 4){
			for(int x = 0; x < dno; x++){
				CreatePlayer4Domino(p4sp);
			}
		}

		// select player that will play first based on main menu options
		SelectFirstToPlay();
	}

	// create new domino for player
	public void CreatePlayerDomino(Vector3 newPos){
		// create new domino and add availabe free domino from stack
		GameObject dom = Instantiate(playerDomino, newPos, Quaternion.Euler(new Vector3(0f,270f,0f))) as GameObject;
		int d = GetNewFreeDomino();
		// set domino texture, color, name and numbers
		dom.GetComponent<Renderer>().material.mainTexture = dominoTexture[d];
		dom.GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 0.6f);
		dom.GetComponent<PlayerDomino>().CreateNameAndNumbers(allDominoes[d], false, 1);
		// add domino to player domino satck
		playerD.Add(allDominoes[d]);
		// new offset
		p1sp.x += 1.4f;
	}

	// create CPU dominoes
	void CreatePlayer2Domino(Vector3 newPos){
		GameObject dom = Instantiate(cpuDomino, newPos, Quaternion.Euler(new Vector3(180f,0f,0f))) as GameObject;
		int d = GetNewFreeDomino();
		dom.GetComponent<Renderer>().material.mainTexture = dominoTexture[d];
		dom.GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 0.6f);
		dom.GetComponent<PlayerDomino>().CreateNameAndNumbers(allDominoes[d], true, 2);
		cpu2D.Add(allDominoes[d]);
		p2sp.z += 1.4f;
	}

	void CreatePlayer3Domino(Vector3 newPos){
		GameObject dom = Instantiate(cpuDomino, newPos, Quaternion.Euler(new Vector3(180f,0f,0f))) as GameObject;
		int d = GetNewFreeDomino();
		dom.GetComponent<Renderer>().material.mainTexture = dominoTexture[d];
		dom.GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 0.6f);
		dom.GetComponent<PlayerDomino>().CreateNameAndNumbers(allDominoes[d], true, 3);
		cpu3D.Add(allDominoes[d]);
		p3sp.z += 1.4f;
	}

	void CreatePlayer4Domino(Vector3 newPos){
		GameObject dom = Instantiate(cpuDomino, newPos, Quaternion.Euler(new Vector3(180f,270f,0f))) as GameObject;
		int d = GetNewFreeDomino();
		dom.GetComponent<Renderer>().material.mainTexture = dominoTexture[d];
		dom.GetComponent<Renderer>().material.color = new Color(0.6f, 0.6f, 0.6f);
		dom.GetComponent<PlayerDomino>().CreateNameAndNumbers(allDominoes[d], true, 4);
		cpu4D.Add(allDominoes[d]);
		p4sp.x += 1.4f;
	}

	// select player that plays first
	void SelectFirstToPlay(){
		// if first to play is in winner mode select player to play first
		if(setScr.f2p == "winner"){
			switch(setScr.lastRoundWinner){
			case 0:
				playerTurn = PlayerTurn.Player1;
				break;
			case 1:
				playerTurn = PlayerTurn.Player1;
				break;
			case 2:
				playerTurn = PlayerTurn.Player2;
				cpuScr.PlayCPUMove();
				break;
			case 3:
				playerTurn = PlayerTurn.Player3;
				cpuScr.PlayCPUMove();
				break;
			case 4:
				playerTurn = PlayerTurn.Player4;
				cpuScr.PlayCPUMove();
				break;
			}
		}else{
			// if double mode select player with highest double
			SelectWithHighestDouble();
		}
	}

	// select player with highest double
	void SelectWithHighestDouble(){
		// default player selection
		int selPlayer = 1;
		int hValue = 0;

		// loop thru all player dominoes
		for(int x = 0; x < playerD.Count; x++){
			string n = (string)playerD[x];
			// get domino numbers
			int n1 = GameObject.Find(n).GetComponent<PlayerDomino>().no1;
			int n2 = GameObject.Find(n).GetComponent<PlayerDomino>().no2;
			// compare numbers
			if(n1 == n2){
				int total = n1 + n2;
				// compare with highest
				if(total > hValue){
					// set new highest and set new player to play first
					hValue = total;
					selPlayer = 1;
				}
			}
		}

		if(playerNo == 3 || playerNo == 4){
			for(int x = 0; x < cpu2D.Count; x++){
				string n = (string)cpu2D[x];
				int n1 = GameObject.Find(n).GetComponent<PlayerDomino>().no1;
				int n2 = GameObject.Find(n).GetComponent<PlayerDomino>().no2;
				if(n1 == n2){
					int total = n1 + n2;
					if(total > hValue){
						hValue = total;
						selPlayer = 2;
					}
				}
			}
		}
		if(playerNo == 3 || playerNo == 4){
			for(int x = 0; x < cpu3D.Count; x++){
				string n = (string)cpu3D[x];
				int n1 = GameObject.Find(n).GetComponent<PlayerDomino>().no1;
				int n2 = GameObject.Find(n).GetComponent<PlayerDomino>().no2;
				if(n1 == n2){
					int total = n1 + n2;
					if(total > hValue){
						hValue = total;
						selPlayer = 3;
					}
				}
			}
		}

		if(playerNo == 2 || playerNo == 4){
			for(int x = 0; x < cpu4D.Count; x++){
				string n = (string)cpu4D[x];
				int n1 = GameObject.Find(n).GetComponent<PlayerDomino>().no1;
				int n2 = GameObject.Find(n).GetComponent<PlayerDomino>().no2;
				if(n1 == n2){
					int total = n1 + n2;
					if(total > hValue){
						hValue = total;
						selPlayer = 4;
					}
				}
			}
		}

		// select player with highest double and set him to play first
		switch(selPlayer){
		case 1:
			playerTurn = PlayerTurn.Player1;
			break;
		case 2:
			playerTurn = PlayerTurn.Player2;
			cpuScr.PlayCPUMove();
			break;
		case 3:
			playerTurn = PlayerTurn.Player3;
			cpuScr.PlayCPUMove();
			break;
		case 4:
			playerTurn = PlayerTurn.Player4;
			cpuScr.PlayCPUMove();
			break;
		}
	}

	// get next free domino
	int GetNewFreeDomino(){
		// get new random domino
		int nd = Random.Range(0,28);

		// test if domino is available in the stack
		if(!usedDominoes[nd]){
			usedDominoes[nd] = true;
			return nd;
		}else{
			nd = GetNewFreeDomino();
		}
		return nd;
	}

	// clear old and create new possible moves
	public void CreatePossibleMoves(){
		ClearMoves();
		CreateMoves();
	}

	// create move dominoes
	void CreateMoves(){
		// if no domino has been placed on the board
		if(noPlaced < 1){
			if(pNo1 == pNo2){
				Instantiate(emptyDomino, new Vector3(0f, 0f, 0f), Quaternion.Euler(new Vector3(0f, 90f, 0f)));
			}else{
				Instantiate(emptyDomino, new Vector3(0f, 0f, 0f), Quaternion.Euler(new Vector3(0f, 0f, 0f)));
			}
		}else{
			// get all dominos on the board
			GameObject[] placedDominoes = GameObject.FindGameObjectsWithTag("domino") as GameObject[];
		
			// create possible moves for every domino
			foreach(GameObject pd in placedDominoes){
				pd.GetComponent<Domino>().CreateEmpty();
			}
		}
	}

	// clear all possibe moves (empty dominoes)
	public void ClearMoves(){
		GameObject[] emptyDominoes = GameObject.FindGameObjectsWithTag("empty") as GameObject[];
		if(emptyDominoes.Length > 0){
			foreach(GameObject d in emptyDominoes){
				DestroyImmediate(d);
			}
		}
	}

	// remove possible move
	public void DestroySelection(){
		// remove played domino
		RemoveFromBoard();
		DestroyImmediate(selected);

		// reset selected domino numbers and available numbers
		pNo1 = -1;
		pNo2 = -1;
		aNo1 = -1;
		aNo2 = -1;

		// reset number of turns passed
		noPassed = 0;

		// clear moves, get available numbers, reposition dominoes that are left in players hand, check if there is a winner
		ClearMoves();
		GetANumbers();
		RepositionRemove();
		TestWinner();

		// if no winner change player turn or show remaining dominoes numbers
		if(!gameOver){
			ChangeTurn();
		}else{
			FlipDominoes();
		}
	}

	// pass turn/no moves
	public void PlayPass(){
		pNo1 = -1;
		pNo2 = -1;
		aNo1 = -1;
		aNo2 = -1;

		noPassed++;

		ClearMoves();
		GetANumbers();
		TestWinner();
		if(!gameOver){
			ChangeTurn();
		}else{
			FlipDominoes();
		}
	}
	
	// remove from player stack and from board
	void RemoveFromBoard(){
		// get selected domino name
		string n = selected.name;
		// select player turn
		switch(playerTurn){
		case PlayerTurn.Player1: 
			// loop all remaining dominoes in player stack
			for(int x = 0; x < playerD.Count; x++){
				// get player stack position
				string pl = (string)playerD[x];
				// if same domino remove it from stack
				if(pl == n){
					playerD.RemoveAt(x);
				}
			}
			break;
		case PlayerTurn.Player2:
			for(int x = 0; x < cpu2D.Count; x++){
				string pl = (string)cpu2D[x];
				
				if(pl == n){
					cpu2D.RemoveAt(x);
				}
			}
			break;
		case PlayerTurn.Player3:
			for(int x = 0; x < cpu3D.Count; x++){
				string pl = (string)cpu3D[x];
				
				if(pl == n){
					cpu3D.RemoveAt(x);
				}
			}
			break;
		case PlayerTurn.Player4:
			for(int x = 0; x < cpu4D.Count; x++){
				string pl = (string)cpu4D[x];
				
				if(pl == n){
					cpu4D.RemoveAt(x);
				}
			}
			break;
		}
	}

	// get available numbers
	public void GetANumbers(){
		// get all dominoes on board
		GameObject[] placedDominoes = GameObject.FindGameObjectsWithTag("domino") as GameObject[];

		// loop all and get available play side
		foreach(GameObject pd in placedDominoes){
			pd.GetComponent<Domino>().GetFreeNumbers();
		}
	}

	// change move turn
	public void ChangeTurn(){
		// select current play turn
		switch(playerTurn){
		case PlayerTurn.Player1:
			// select number of players
			switch(playerNo){
			case 1:
				// FOR TESTING ONLY
				//playerTurn = PlayerTurn.Player1;
				break;
			case 2:
				// change player turn
				playerTurn = PlayerTurn.Player4;
				// make cpu play move
				cpuScr.PlayCPUMove();
				break;
			case 3:
				// select play direction, set player turn, make cpu play move
				if(setScr.direction == "cw"){
				playerTurn = PlayerTurn.Player2;
				cpuScr.PlayCPUMove();
				}else{
					playerTurn = PlayerTurn.Player3;
					cpuScr.PlayCPUMove();
				}
				break;
			case 4:
				if(setScr.direction == "cw"){
					playerTurn = PlayerTurn.Player2;
					cpuScr.PlayCPUMove();
				}else{
					playerTurn = PlayerTurn.Player3;
					cpuScr.PlayCPUMove();
				}
				break;
			}
			break;
		case PlayerTurn.Player2:
			switch(playerNo){
			case 3:
				if(setScr.direction == "cw"){
					playerTurn = PlayerTurn.Player3;
					cpuScr.PlayCPUMove();
				}else{
					playerTurn = PlayerTurn.Player1;
					cpuScr.PlayCPUMove();
				}
				break;
			case 4:
				if(setScr.direction == "cw"){
					playerTurn = PlayerTurn.Player4;
					cpuScr.PlayCPUMove();
				}else{
					playerTurn = PlayerTurn.Player1;
					cpuScr.PlayCPUMove();
				}
				break;
			}
			break;
		case PlayerTurn.Player3:
			switch(playerNo){
			case 3:
				if(setScr.direction == "cw"){
					playerTurn = PlayerTurn.Player1;
					cpuScr.PlayCPUMove();
				}else{
					playerTurn = PlayerTurn.Player2;
					cpuScr.PlayCPUMove();
				}
				break;
			case 4:
				if(setScr.direction == "cw"){
					playerTurn = PlayerTurn.Player1;
					cpuScr.PlayCPUMove();
				}else{
					playerTurn = PlayerTurn.Player4;
					cpuScr.PlayCPUMove();
				}
				break;
			}
			break;
		case PlayerTurn.Player4:
			switch(playerNo){
			case 2:
				playerTurn = PlayerTurn.Player1;
				break;
			case 3:
				playerTurn = PlayerTurn.Player3;
				cpuScr.PlayCPUMove();
				break;
			case 4:
				if(setScr.direction == "cw"){
					playerTurn = PlayerTurn.Player3;
					cpuScr.PlayCPUMove();
				}else{
					playerTurn = PlayerTurn.Player2;
					cpuScr.PlayCPUMove();
				}
				break;
			}
			break;
		}
	}
	
	// reposition dominoes
	public void RepositionAdd(){
		// count free dominoes
		int nfd = CountFreeDominoes();
		// select player
		switch(playerTurn){
		case PlayerTurn.Player1:
			// create new domino
			CreatePlayerDomino(p1sp);
			// reposition dominoes
			RepositionRemove();
			break;
		case PlayerTurn.Player2:
			// if there are free dominoes and game type is "draw"
			if(nfd > 0 && setScr.gameType == "draw"){
				CreatePlayer2Domino(p2sp);
				RepositionRemove();
				// play cpu move
				cpuScr.PlayCPUMove();
			}else{
				// play pass
				cpuScr.PassMove();
			}
			break;
		case PlayerTurn.Player3:
			if(nfd > 0 && setScr.gameType == "draw"){
				CreatePlayer3Domino(p3sp);
				RepositionRemove();
				cpuScr.PlayCPUMove();
			}else{
				cpuScr.PassMove();
			}
			break;
		case PlayerTurn.Player4:
			if(nfd > 0 && setScr.gameType == "draw"){
				CreatePlayer4Domino(p4sp);
				RepositionRemove();
				cpuScr.PlayCPUMove();
			}else{
				cpuScr.PassMove();
			}
			break;
		}
	}

	// reposition dominoes when removing/adding
	public void RepositionRemove(){
		// select player turn
		switch(playerTurn){
		case PlayerTurn.Player1:
			// get number of dominoes
			int p1c = playerD.Count;
			if(p1c > 1){
				// create offset
				float sp = ((p1c-1) * -1.4f)/2;
				p1sp.x = sp;

				// loop thru all dominoes
				for(int x = 0; x < p1c; x++){
					// get position
					Vector3 np = p1sp;
					// get gomino name from stack
					string n = (string)playerD[x];
					// find domino and move it to new position
					GameObject.Find(n).GetComponent<PlayerDomino>().MoveToPosition(np);
					// create offset
					p1sp.x += 1.4f;
				}
			}else if(p1c == 1){
				// get start position 
				Vector3 np = p1sp;
				// create offset
				np.x = 0.0f;
				// get domino name from stack
				string n = (string)playerD[0];
				// find domino and move it to new position
				GameObject.Find(n).GetComponent<PlayerDomino>().MoveToPosition(np);
			}
			break;
		case PlayerTurn.Player2:
			int p2c = cpu2D.Count;
			if(p2c > 1){
				float sp = ((p2c-1) * -1.4f)/2;
				p2sp.z = sp;
				
				for(int x = 0; x < p2c; x++){
					Vector3 np = p2sp;
					string n = (string)cpu2D[x];
					GameObject.Find(n).GetComponent<PlayerDomino>().MoveToPosition(np);
					p2sp.z += 1.4f;
				}
			}else if(p2c == 1){
				Vector3 np = p2sp;
				np.z = 0.0f;
				string n = (string)cpu2D[0];
				GameObject.Find(n).GetComponent<PlayerDomino>().MoveToPosition(np);
			}
			break;
		case PlayerTurn.Player3:
			int p3c = cpu3D.Count;
			if(p3c > 1){
				float sp = ((p3c-1) * -1.4f)/2;
				p3sp.z = sp;
				
				for(int x = 0; x < p3c; x++){
					Vector3 np = p3sp;
					string n = (string)cpu3D[x];
					GameObject.Find(n).GetComponent<PlayerDomino>().MoveToPosition(np);
					p3sp.z += 1.4f;
				}
			}else if(p3c == 1){
				Vector3 np = p3sp;
				np.z = 0.0f;
				string n = (string)cpu3D[0];
				GameObject.Find(n).GetComponent<PlayerDomino>().MoveToPosition(np);
			}
			break;
		case PlayerTurn.Player4:
			int p4c = cpu4D.Count;
			if(p4c > 1){
				float sp = ((p4c-1) * -1.4f)/2;
				p4sp.x = sp;
				
				for(int x = 0; x < p4c; x++){
					Vector3 np = p4sp;
					string n = (string)cpu4D[x];
					GameObject.Find(n).GetComponent<PlayerDomino>().MoveToPosition(np);
					p4sp.x += 1.4f;
				}
			}else if(p4c == 1){
				Vector3 np = p4sp;
				np.x = 0.0f;
				string n = (string)cpu4D[0];
				GameObject.Find(n).GetComponent<PlayerDomino>().MoveToPosition(np);
			}
			break;
		}
	}

	// check is there a round winner
	void TestWinner(){
		// test if there is a draw
		bool draw = TestDraw();

		// if there is a draw
		if(draw){
			// set game over and get all players hand total
			gameOver = true;
			int p1hand = GetPlayerHandScore(playerD);
			int p2hand = GetPlayerHandScore(cpu2D);
			int p3hand = GetPlayerHandScore(cpu3D);
			int p4hand = GetPlayerHandScore(cpu4D);

			//winenr
			int winner = 0;

			// select number of players
			switch(playerNo){
			case 2:
				// compare total hands, set winner and show score panel
				if(p1hand < p4hand){
					winner = 1;
				}else{
					winner = 2;
				}
				uiScr.ShowScoresPanel(winner, p1hand, p4hand, 0, 0);
				break;
			case 3:
				if(p1hand < p2hand && p1hand < p3hand){
					winner = 1;
				}else if(p2hand < p1hand && p2hand < p3hand){
					winner = 2;
				}else if(p3hand < p1hand && p3hand < p2hand){
					winner = 3;
				}else{
					winner = 1;
				}
				uiScr.ShowScoresPanel(winner, p1hand, p2hand, p3hand, 0);
				break;
			case 4:
				if(p4hand < p1hand && p4hand < p2hand && p4hand < p3hand){
					winner = 4;
				}else if(p3hand < p1hand && p3hand < p2hand && p3hand < p4hand){
					winner = 3;
				}else if(p2hand < p1hand && p2hand < p3hand && p2hand < p4hand){
					winner = 2;
				}else if(p1hand < p2hand && p1hand < p3hand && p1hand < p4hand){
					winner = 1;
				}else{
					winner = 1;
				}
				uiScr.ShowScoresPanel(winner, p1hand, p2hand, p3hand, p4hand);
				break;
			}
		}else{
			// test if there is a winner
			int winner = TestNoDominoesLeft();

			// if there is a winner
			if(winner > 0){
				// set game over
				gameOver = true;
				// get all players hands
				int p1hand = GetPlayerHandScore(playerD);
				int p2hand = GetPlayerHandScore(cpu2D);
				int p3hand = GetPlayerHandScore(cpu3D);
				int p4hand = GetPlayerHandScore(cpu4D);

				// select number of players in the game
				switch(playerNo){
				case 2:
					// show panel based on number of players
					uiScr.ShowScoresPanel(winner, p1hand, p4hand, 0, 0);
					break;
				case 3:
					uiScr.ShowScoresPanel(winner, p1hand, p2hand, p3hand, 0);
					break;
				case 4:
					uiScr.ShowScoresPanel(winner, p1hand, p2hand, p3hand, p4hand);
					break;
				}
			}
		}
	}

	// check if there is a draw
	bool TestDraw(){
		bool isDraw = false;
		// if all players are out of moves
		if(noPassed == playerNo){
			isDraw = true;
		}
		return isDraw;
	}

	// check if player has used all his dominoes
	int TestNoDominoesLeft(){
		// winner
		int w = 0;

		// select number of players in the game
		switch(playerNo){
		case 2:
			if(playerD.Count == 0){
				w = 1;
			}
			if(cpu4D.Count == 0){
				w = 2;
			}
			break;
		case 3:
			if(playerD.Count == 0){
				w = 1;
			}
			if(cpu2D.Count == 0){
				w = 2;
			}
			if(cpu3D.Count == 0){
				w = 3;
			}
			break;
		case 4:
			if(playerD.Count == 0){
				w = 1;
			}
			if(cpu2D.Count == 0){
				w = 2;
			}
			if(cpu3D.Count == 0){
				w = 3;
			}
			if(cpu4D.Count == 0){
				w = 4;
			}
			break;
		}
		// return winner
		return w;
	}

	// flip all dominos that are left on the board
	void FlipDominoes(){
		if(cpu2D.Count > 0){
			for(int x = 0; x < cpu2D.Count; x++){
				string n = (string)cpu2D[x];
				GameObject.Find(n).transform.Rotate(0f,0f,180f,Space.Self);
			}
		}
		if(cpu3D.Count > 0){
			for(int x = 0; x < cpu3D.Count; x++){
				string n = (string)cpu3D[x];
				GameObject.Find(n).transform.Rotate(0f,0f,180f,Space.Self);
			}
		}
		if(cpu4D.Count > 0){
			for(int x = 0; x < cpu4D.Count; x++){
				string n = (string)cpu4D[x];
				GameObject.Find(n).transform.Rotate(0f,0f,180f,Space.Self);
			}
		}
	}

	// calculate score
	int GetPlayerHandScore(ArrayList al){
		// dominoes left in hand total
		int totalHand = 0;
		if(al.Count > 0){
			for(int x = 0; x < al.Count; x++){
				// get domino number
				string dn = (string)al[x];
				// get domino first number
				string n = dn[0].ToString();
				// add number to total
				totalHand  += int.Parse(n);
				// get domino second number
				n = dn[1].ToString();
				// add to total
				totalHand  += int.Parse(n);
			}
		}
		// return total hand score
		return totalHand;
	}

	// get number of unused dominoes
	public int CountFreeDominoes(){
		int dc = 0;
		
		foreach(bool b in usedDominoes){
			if(b == false){
				dc++;
			}
		}
		return dc;
	}
}
