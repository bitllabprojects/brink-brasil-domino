﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenu : MonoBehaviour {
	
	int nop = 2;														// default number of players
	string pd = "cw";												// default play direction
	string gt = "draw";											// default play mode
	int tScore = 100;												// default target score
	string f2p = "highest";										// default first to play
	int dpp = 5;														// default dominoes per player

	// ui dropdowns
	public Dropdown noOfPlayers;						// number of players
	public Dropdown playDirection;					// play direction
	public Dropdown gameType;						// game type
	public Dropdown totalScore;							// total score
	public Dropdown firstToPlay;							// first to play
	public Dropdown dominoesPerPlayer;		// dominoes per player

	Setup setScr;														// script
	
	// 
	void Start(){
		setScr = GameObject.Find("Setup").GetComponent<Setup>();
		// get setup values
		GetValues();
		// set dropdown values
		SetValues();
	}
	
	// set and load new game
	public void StartNewGame(){
		setScr.playerNo = nop;
		setScr.direction = pd;
		setScr.gameType = gt;
		setScr.targetScore = tScore;
		setScr.f2p = f2p;
		setScr.dominoStartNo = dpp;
		//Application.LoadLevel("Game");
		SceneManager.LoadScene("Game");
	}

	// set number of players from dropdown
	public void SetNumberOfPlayers(int a){
		switch(a){
		case 0:
			nop = 2;
			break;
		case 1:
			nop = 3;
			break;
		case 2:
			nop = 4;
			break;
		}
	}

	// set play direction
	public void SetPlayDirection(int a){
		switch(a){
		case 0:
			pd = "cw";
			break;
		case 1:
			pd = "ccw";
			break;
		}
	}

	// set game type
	public void SetGameType(int a){
		switch(a){
		case 0:
			gt = "draw";
			break;
		case 1:
			gt = "pass";
			break;
		}
	}

	// set target score
	public void SetTargetScore(int a){
		switch(a){
		case 0:
			tScore = 100;
			break;
		case 1:
			tScore = 150;
			break;
		case 2:
			tScore = 200;
			break;
		case 3:
			tScore = 300;
			break;
		case 4:
			tScore = 500;
			break;
		}
	}

	// set first to play
	public void SetFirstToPlay(int a){
		switch(a){
		case 0:
			f2p = "highest";
			break;
		case 1:
			f2p = "winner";
			break;
		}
	}

	// set dominoes per player
	public void SetDominoerPerPlayer(int a){
		switch(a){
		case 0:
			dpp = 5;
			break;
		case 1:
			dpp = 6;
			break;
		case 2:
			dpp = 7;
			break;
		}
	}

	// get values from setup
	void GetValues(){
		nop = setScr.playerNo;
		pd = setScr.direction;
		gt = setScr.gameType;
		tScore = setScr.targetScore;
		f2p = setScr.f2p;
		dpp = setScr.dominoStartNo;
	}

	// set all values
	void SetValues(){
		switch(nop){
		case 2:
			noOfPlayers.value = 0;
			break;
		case 3:
			noOfPlayers.value = 1;
			break;
		case 4:
			noOfPlayers.value = 2;
			break;
		}

		switch(pd){
		case "cw":
			playDirection.value = 0;
			break;
		case "ccw":
			playDirection.value = 1;
			break;
		}

		switch(gt){
		case "draw":
			gameType.value = 0;
			break;
		case "pass":
			gameType.value = 1;
			break;
		}

		switch(tScore){
		case 100:
			totalScore.value = 0;
			break;
		case 150:
			totalScore.value = 1;
			break;
		case 200:
			totalScore.value = 2;
			break;
		case 300:
			totalScore.value = 3;
			break;
		case 500:
			totalScore.value = 4;
			break;
		}

		switch(f2p){
		case "highest":
			firstToPlay.value = 0;
			break;
		case "winner":
			firstToPlay.value = 1;
			break;
		}

		switch(dpp){
		case 5:
			dominoesPerPlayer.value = 0;
			break;
		case 6:
			dominoesPerPlayer.value = 1;
			break;
		case 7:
			dominoesPerPlayer.value = 2;
			break;
		}
	}
}
