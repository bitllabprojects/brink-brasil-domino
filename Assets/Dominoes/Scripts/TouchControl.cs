﻿using UnityEngine;
using System.Collections;

public class TouchControl : MonoBehaviour {

	Touch[] touch;
	
	// my custom touch script
	void Update () {
		touch = Input.touches;
		if(touch.Length == 1){
			if(touch[0].phase == TouchPhase.Began){
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if(Physics.Raycast(ray, out hit)){
					if(hit.collider.tag == "empty"){
						hit.collider.GetComponent<Empty>().GetTouch();
					}else if(hit.collider.tag == "playerdomino"){
						hit.collider.GetComponent<PlayerDomino>().OnFinger();
					}
				}
			}
			if(touch[0].phase == TouchPhase.Moved){
				// finger has moved
			}
			if(touch[0].phase == TouchPhase.Ended){

			}
			if(touch[0].phase == TouchPhase.Canceled){

			}
			if(touch[0].phase == TouchPhase.Canceled){

			}
		}
	}
}
