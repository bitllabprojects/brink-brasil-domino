﻿using UnityEngine;
using System.Collections;


public class Domino : MonoBehaviour{
	
	public Transform n1Part;								// number raycast
	public Transform n2Part;								// 
	public int n1;													// number 1
	public int n2;													// number 2
	float rl1 = 1.0f;													// ray length
	float rl2 = 1.0f;													//
	Game gscript;													//

	
	// get scripts
	void Awake(){
		gscript = GameObject.Find("Game").GetComponent<Game>();
	}

	// set domino number 1 and number 2
	public void SetDomino(int a, int b){
		n1Part.GetComponent<NumberPart>().num = a;
		n2Part.GetComponent<NumberPart>().num = b;
		// get numbers for domino
		GetNumbers();
		// set domino textures
		SetTextures();
	}

	// set domino number and rotation
	public void SetDomino(int a, int b, int c){
		// compare domino side and available move number
		if(c == a){
			// test right direction
			if(Physics.Raycast(n1Part.position, n1Part.forward, 1.0f)){
				// set numbers based on direction
				n1Part.GetComponent<NumberPart>().num = a;
				n2Part.GetComponent<NumberPart>().num = b;
				// set is this domino disabled
				if(a != b){
					n1Part.GetComponent<NumberPart>().isDisabled = true;
				}
			}else{
				n1Part.GetComponent<NumberPart>().num = b;
				n2Part.GetComponent<NumberPart>().num = a;
				if(a != b){
					n2Part.GetComponent<NumberPart>().isDisabled = true;
				}
			}
		}
		if(c == b){
			if(Physics.Raycast(n1Part.position, n1Part.forward, 1.0f)){
				n1Part.GetComponent<NumberPart>().num = b;
				n2Part.GetComponent<NumberPart>().num = a;
				if(a != b){
					n1Part.GetComponent<NumberPart>().isDisabled = true;
				}
			}else{
				n1Part.GetComponent<NumberPart>().num = a;
				n2Part.GetComponent<NumberPart>().num = b;
				if(a != b){
					n2Part.GetComponent<NumberPart>().isDisabled = true;
				}
			}
		}
		// get numbers and set textures
		GetNumbers();
		SetTextures();
	}

	// get number 1 and number 2 from part 1 and part 2
	void GetNumbers(){
		n1 = n1Part.GetComponent<NumberPart>().num;
		n2 = n2Part.GetComponent<NumberPart>().num;
	}

	// set textures for part 1 and part 2
	void SetTextures(){
		n1Part.GetComponent<Renderer>().material.mainTexture = gscript.playTexture[n1];
		n2Part.GetComponent<Renderer>().material.mainTexture = gscript.playTexture[n2];
	}

	// create empty dominoes
	public void CreateEmpty(){
		if(n1 == n2){
			// create possible moves for double domino
			CreateFromDouble();
		}else{
			// create possible moves for normal domino
			CreateFromNormal();
		}
	}

	// create for double domino
	void CreateFromDouble(){
		// get domino rotation
		Vector3 drot = transform.rotation.eulerAngles;
		// get domino position
		Vector3 pos = transform.position;
		// rotate by 90deg.
		drot.y += 90.0f;

		RaycastHit hit;
		// test domino position and domino row bend
		if((pos.x > -7.9f && pos.x < 0.1f && gscript.toLeft == 0) || (pos.z < 5.9f && gscript.toLeft == 1 && pos.z > -0.1f) || (pos.x < 10.9f && gscript.toLeft == 2 && pos.z > 0.1f)){
			// if domino parts not disabled
			if(!n1Part.GetComponent<NumberPart>().isDisabled && !n2Part.GetComponent<NumberPart>().isDisabled){
				// test for dominoes
				if(!Physics.Raycast(transform.position, transform.forward, out hit, 1.0f)){
					// compare domino numbers and available numbers
					if(n1 == gscript.pNo1 || n1 == gscript.pNo2){
						// create position for new domino
						Vector3 newPos = transform.position + transform.forward * 1.5f;
						// create empty move domino
						CreateEmptyDomino(null, newPos, Quaternion.Euler(drot), n1, null,null, false, false);
					}
				}
				if(!Physics.Raycast(transform.position, -transform.forward, out hit, 1.0f)){
					if(n1 == gscript.pNo1 || n1 == gscript.pNo2){
						Vector3 newPos = transform.position - transform.forward * 1.5f;
						CreateEmptyDomino(null, newPos, Quaternion.Euler(drot), n1, null,null, false, false);
					}
				}
			}
		}else if((pos.x < 7.9f && pos.x > -0.1f && gscript.toRight == 0) || (pos.z > -5.9f && gscript.toRight == 1 && pos.z < -0.1f) || (pos.x > -10.9f && gscript.toRight == 2 && pos.z < -0.1f)){
			if(!n1Part.GetComponent<NumberPart>().isDisabled && !n2Part.GetComponent<NumberPart>().isDisabled){
				if(!Physics.Raycast(transform.position, transform.forward, out hit, 1.0f)){
					if(n1 == gscript.pNo1 || n1 == gscript.pNo2){
						Vector3 newPos = transform.position + transform.forward * 1.5f;
						CreateEmptyDomino(null, newPos, Quaternion.Euler(drot), n1, null,null, false, false);
					}
				}
				if(!Physics.Raycast(transform.position, -transform.forward, out hit, 1.0f)){
					if(n1 == gscript.pNo1 || n1 == gscript.pNo2){
						Vector3 newPos = transform.position - transform.forward * 1.5f;
						CreateEmptyDomino(null, newPos, Quaternion.Euler(drot), n1, null,null, false, false);
					}
				}
			}
		}else{
			if(!n1Part.GetComponent<NumberPart>().isDisabled && !n2Part.GetComponent<NumberPart>().isDisabled){
				if(n1 == gscript.pNo1 || n1 == gscript.pNo2){
					SelectDirection1();
					SelectDirection2();
				}
			}
		}
	}

	// select part 1 direction
	void SelectDirection1(){
		Vector3 dpos = transform.position;
		Vector3 pos = n1Part.position;

		// test part position
		if(dpos.x < -7.9f || dpos.z > 5.9f){
			// select bend
			switch(gscript.toLeft){
			case 0:
				if(pos.z > dpos.z){
					Vector3 newPos = n1Part.position + n1Part.forward*1.5f;
					CreateEmptyDomino(null, newPos, transform.rotation, n1, n1Part.gameObject, n2Part.gameObject, true, false);
				}
				break;
			case 1:
				if(pos.x > dpos.x){
					Vector3 newPos = n1Part.position + n1Part.forward*1.5f;
					CreateEmptyDomino(null, newPos, transform.rotation, n1, n1Part.gameObject, n2Part.gameObject, true, false);
				}
				break;
			}
		}else if(dpos.x > 7.9 || dpos.z < -5.9f){
			switch(gscript.toRight){
			case 0:
				if(pos.z < dpos.z){
					Vector3 newPos = n1Part.position + n1Part.forward*1.5f;
					CreateEmptyDomino(null, newPos, transform.rotation, n1, n1Part.gameObject, n2Part.gameObject, false, true);
				}
				break;
			case 1:
				if(pos.x < dpos.x){
					Vector3 newPos = n1Part.position + n1Part.forward*1.5f;
					CreateEmptyDomino(null, newPos, transform.rotation, n1, n1Part.gameObject, n2Part.gameObject, false, true);
				}
				break;
			}
		}
	}

	// select part 2 direction
	void SelectDirection2(){
		Vector3 dpos = transform.position;
		Vector3 pos = n2Part.position;

		if(dpos.x < -7.9f || dpos.z > 5.9f){
			switch(gscript.toLeft){
			case 0:
				if(pos.z > dpos.z){
					Vector3 newPos = n2Part.position + n2Part.forward*1.5f;
					CreateEmptyDomino(null, newPos, transform.rotation, n2, n2Part.gameObject, n2Part.gameObject, true, false);
				}
				break;
			case 1:
				if(pos.x > dpos.x){
					Vector3 newPos = n2Part.position + n2Part.forward*1.5f;
					CreateEmptyDomino(null, newPos, transform.rotation, n2, n2Part.gameObject, n2Part.gameObject, true, false);
				}
				break;
			}
		}else if(dpos.x > 7.9 || dpos.z < -5.9f){
			switch(gscript.toRight){
			case 0:
				if(pos.z < dpos.z){
					Vector3 newPos = n2Part.position + n2Part.forward*1.5f;
					CreateEmptyDomino(null, newPos, transform.rotation, n2, n2Part.gameObject, n2Part.gameObject, false ,true);
				}
				break;
			case 1:
				if(pos.x < dpos.x){
					Vector3 newPos = n2Part.position + n2Part.forward*1.5f;
					CreateEmptyDomino(null, newPos, transform.rotation, n2, n2Part.gameObject, n2Part.gameObject, false, true);
				}
				break;
			}
		}
	}

	// create for normal
	void CreateFromNormal(){
		Vector3 drot = transform.rotation.eulerAngles;
		drot.y += 90.0f;

		if(gscript.pNo1 == gscript.pNo2){
			if(!n1Part.GetComponent<NumberPart>().isDisabled){
				if(gscript.pNo1 == n1 || gscript.pNo2 == n1){
					Vector3 newPos = n1Part.position + n1Part.forward*1.0f;
					CreateEmptyDomino(n1Part, newPos, Quaternion.Euler(drot), n1, n1Part.gameObject, null, false, false);
				}
			}
			if(!n2Part.GetComponent<NumberPart>().isDisabled){
				if(gscript.pNo1 == n2 || gscript.pNo2 == n2){
					Vector3 newPos = n2Part.position + n2Part.forward*1.0f;
					CreateEmptyDomino(n2Part, newPos, Quaternion.Euler(drot), n2, n2Part.gameObject, null, false, false);
				}
			}
		}else{
			if(!n1Part.GetComponent<NumberPart>().isDisabled){
				Vector3 partPos = n1Part.position;
				if((partPos.x > -7.9f && partPos.x < -0.1f && gscript.toLeft == 0) || (partPos.z < 5.9f && gscript.toLeft == 1 && partPos.z > 0.1f) || (partPos.x < 10.9f && gscript.toLeft == 2 && partPos.z > 0.1f)){
					if(gscript.pNo1 == n1 || gscript.pNo2 == n1){
						Vector3 newPos = n1Part.position + n1Part.forward * 1.5f;
						CreateEmptyDomino(n1Part, newPos, transform.rotation, n1, n1Part.gameObject, null, false, false);
					}
				}else if((partPos.x < 7.9f && partPos.x > 0.1f && gscript.toRight == 0) || (partPos.z > -5.9f && gscript.toRight == 1 && partPos.z < -0.1f) || (partPos.x > -10.9f && gscript.toRight == 2 && partPos.z < -0.1f)){
					if(gscript.pNo1 == n1 || gscript.pNo2 == n1){
						Vector3 newPos = n1Part.position + n1Part.forward * 1.5f;
						CreateEmptyDomino(n1Part, newPos, transform.rotation, n1, n1Part.gameObject, null, false, false);
					}
				}else{
					if(gscript.pNo1 == n1 || gscript.pNo2 == n1){
						Vector3 newPos = n1Part.position + n1Part.right * 1.5f;
						bool tlf = false;
						bool trg = false;
						if(partPos.x < -7.9f || partPos.z > 5.9f){
							tlf = true;
						}else if(partPos.x > 7.9 || partPos.z < -5.9f){
							trg = true;
						}
						CreateEmptyDomino(n1Part, newPos, Quaternion.Euler(drot), n1, n1Part.gameObject, null, tlf, trg);
					}
				}
			}
			if(!n2Part.GetComponent<NumberPart>().isDisabled){
				Vector3 partPos = n2Part.position;
				if((partPos.x > -7.9f && partPos.x < -0.1f && gscript.toLeft == 0) || (partPos.z < 5.9f && gscript.toLeft == 1 && partPos.z > 0.1f) || (partPos.x < 7.9f && gscript.toLeft == 2 && partPos.z > 0.1f)){
					if(gscript.pNo1 == n2 || gscript.pNo2 == n2){
						Vector3 newPos = n2Part.position + n2Part.forward * 1.5f;
						CreateEmptyDomino(n2Part, newPos, transform.rotation, n2, n2Part.gameObject, null, false, false);
					}
				}else if((partPos.x < 7.9f && partPos.x > 0.1f && gscript.toRight == 0) || (partPos.z > -5.9f && gscript.toRight == 1 && partPos.z < -0.1f) || (partPos.x > -7.9f && gscript.toRight == 2 && partPos.z < -0.1f)){
					if(gscript.pNo1 == n2 || gscript.pNo2 == n2){
						Vector3 newPos = n2Part.position + n2Part.forward * 1.5f;
						CreateEmptyDomino(n2Part, newPos, transform.rotation, n2, n2Part.gameObject, null, false, false);
					}
				}else{
					if(gscript.pNo1 == n2 || gscript.pNo2 == n2){
						Vector3 newPos = n2Part.position + n2Part.right * 1.5f;
						bool tlf = false;
						bool trg = false;
						if(partPos.x < -7.9f || partPos.z > 5.9f){
							tlf = true;
						}else if(partPos.x > 7.9 || partPos.z < -5.9f){
							trg = true;
						}
						CreateEmptyDomino(n2Part, newPos, Quaternion.Euler(drot), n2, n2Part.gameObject, null, tlf, trg);
					}
				}
			}
		}
	}

	// set turning side
	public void TurnRow(bool a, bool b){
		if(a){
			gscript.toLeft++;
		}
		if(b){
			gscript.toRight++;
		}
	}

	// create empty domino
	void CreateEmptyDomino(Transform part, Vector3 pos, Quaternion rot, int set, GameObject toDis1, GameObject toDis2, bool tl, bool tr){
		GameObject em = Instantiate(gscript.emptyDomino, pos, rot) as GameObject;
		em.GetComponent<Empty>().toSet = set;
		// pass object that will be disabled after move been played
		PassDisableObject(em, toDis1, toDis2, tl, tr);
	}

	// pass object to disable
	void PassDisableObject(GameObject g, GameObject d, GameObject d2, bool tl, bool tr){
		g.GetComponent<Empty>().SetToDisable(d, d2, tl, tr);
	}

	// get numbers that can be placed on this domino
	public void GetFreeNumbers(){
		if(n1 == n2){
			if(!n1Part.GetComponent<NumberPart>().isDisabled && !n2Part.GetComponent<NumberPart>().isDisabled){
				if(!Physics.Raycast(transform.position, transform.forward, 1.0f)){
					if(gscript.aNo1 < 0){
						gscript.aNo1= n1;
					}else if(gscript.aNo1 > 0 || gscript.aNo1 != n1){
						if(gscript.aNo2 < 0){
							gscript.aNo2 = n1;
						}
					}
				}
				if(!Physics.Raycast(transform.position, -transform.forward, 1.0f)){
					if(gscript.aNo1 < 0){
						gscript.aNo1= n1;
					}else if(gscript.aNo1 > 0 || gscript.aNo1 != n1){
						if(gscript.aNo2 < 0){
							gscript.aNo2 = n1;
						}
					}
				}
			}
		}else{
			if(!n1Part.GetComponent<NumberPart>().isDisabled){
				if(!Physics.Raycast(n1Part.position, n1Part.forward, rl1)){
					if(gscript.aNo1 < 0){
						gscript.aNo1= n1;
					}else if(gscript.aNo1 > 0 || gscript.aNo1 != n1){
						if(gscript.aNo2 < 0){
							gscript.aNo2 = n1;
						}
					}
				}
			}
			if(!n2Part.GetComponent<NumberPart>().isDisabled){
				if(!Physics.Raycast(n2Part.position, n2Part.forward, rl2)){
					if(gscript.aNo1 < 0){
						gscript.aNo1= n2;
					}else if(gscript.aNo1 > 0 || gscript.aNo1 != n2){
						if(gscript.aNo2 < 0){
							gscript.aNo2 = n2;
						}
					}
				}
			}
		}
	}
}
