﻿using UnityEngine;
using System.Collections;

public class CPU : MonoBehaviour {

	bool countDelay;						// start count move delay
	bool countPlace;						// start count domino creation delay
	bool countPass;						// start count pass move

	float moveDelay = 1.5f;			// move delay time
	float placeDelay = 1.5f;			// create delay for domino creation
	float passDelay = 1.5f;			// create pass move delay time

	float timerDelay = 0.0f;			// time counter
	float timerPlace = 0.0f;			//
	float timerPass = 0.0f;				//

	UIControl uiScr;							// get scripts
	Game gscript;							// 
	Setup setScr;								//
	
	// get scripts
	void Start () {
		gscript = GameObject.Find("Game").GetComponent<Game>();
		uiScr = GameObject.Find("CanvasGame").GetComponent<UIControl>();
		setScr = GameObject.Find("Setup").GetComponent<Setup>();
	}
	
	// Update is called once per frame
	void Update(){
		// count move delay
		if(countDelay){
			if(timerDelay < moveDelay){
				timerDelay += 1.0f*Time.deltaTime;
			}else{
				timerDelay = 0.0f;
				countDelay = false;
				// select player turn
				switch(gscript.playerTurn){
				case Game.PlayerTurn.Player2:
					// play move
					PlayPlayer2Move();
					break;
				case Game.PlayerTurn.Player3:
					PlayPlayer3Move();
					break;
				case Game.PlayerTurn.Player4:
					PlayPlayer4Move();
					break;
				}
			}
		}
		// place delay timer
		if(countPlace){
			if(timerPlace < placeDelay){
				timerPlace += 1.0f*Time.deltaTime;
			}else{
				timerPlace = 0.0f;
				countPlace = false;
				// play move
				PlaySelectedMove();
			}
		}
		// count no moves
		if(countPass){
			if(timerPass < passDelay){
				timerPass += 1.0f*Time.deltaTime;
			}else{
				timerPass = 0.0f;
				countPass = false;
				// pass move
				gscript.PlayPass();
			}
		}
	}

	// no moves set pass
	public void PassMove(){
		countPass = true;
		// start pass image scale
		uiScr.StartPassScale();
	}

	// play player 2 move
	void PlayPlayer2Move(){
		// if no dominoes placed on board
		if(gscript.noPlaced > 0){
			// get possible moves for cpu
			GetPossibleMoves(gscript.cpu2D);
		}else{
			// if first to play = winner mode
			if(setScr.f2p == "winner"){
				SelectFirstMoveWinner(gscript.cpu2D);
			}else{
				// first to play = highest mode
				SelectHighestDouble(gscript.cpu2D);
			}
		}
	}

	// play player 3 move
	void PlayPlayer3Move(){
		if(gscript.noPlaced > 0){
			GetPossibleMoves(gscript.cpu3D);
		}else{
			if(setScr.f2p == "winner"){
				SelectFirstMoveWinner(gscript.cpu3D);
			}else{
				SelectHighestDouble(gscript.cpu3D);
			}
		}
	}

	// play player 4 move
	void PlayPlayer4Move(){
		if(gscript.noPlaced > 0){
			GetPossibleMoves(gscript.cpu4D);
		}else{
			if(setScr.f2p == "winner"){
				SelectFirstMoveWinner(gscript.cpu4D);
			}else{
				SelectHighestDouble(gscript.cpu4D);
			}
		}
	}

	// start cpu move
	public void PlayCPUMove(){
		// start move delay
		countDelay = true;
	}

	// get all possible moves for player
	void GetPossibleMoves(ArrayList al){
		// create new possible moves list
		ArrayList pm = new ArrayList();

		// count and loop all cpu player dominoes
		for(int x = 0; x < al.Count; x++){
			// get domno name from stack
			string n = (string)al[x];
			// get domino number 1 and number 2
			int n1 = GameObject.Find(n).GetComponent<PlayerDomino>().no1;
			int n2 = GameObject.Find(n).GetComponent<PlayerDomino>().no2;
			// compare values
			if(gscript.aNo1 == n1 || gscript.aNo1 == n2 || gscript.aNo2 == n1 || gscript.aNo2 == n2){
				// add move to list
				pm.Add(n);
			}
		}

		// if any moves
		if(pm.Count > 0){
			// select move to play
			SelectMoveToPlay(pm);
		}else{
			// pass move
			gscript.RepositionAdd();
		}
	}

	// select move to play
	void SelectMoveToPlay(ArrayList al){
		// create new move
		int mtp = Random.Range(0,al.Count);
		// get cpu domino name
		string n = (string)al[mtp];
		// find and select domino
		GameObject.Find(n).GetComponent<PlayerDomino>().OnCPU();
		// start place timer
		countPlace = true;
	}

	// select first move winner game type
	void SelectFirstMoveWinner(ArrayList al){
		// set stack position and highest value
		int arPos = 0;
		int hValue = 0;

		// loop player hand
		for(int x = 0; x < al.Count; x++){
			// get domino name from stack
			string n = (string)al[x];
			// get domino numbers
			int n1 = GameObject.Find(n).GetComponent<PlayerDomino>().no1;
			int n2 = GameObject.Find(n).GetComponent<PlayerDomino>().no2;
			// get domino total value
			int total = n1 + n2;
			// compare with highest value
			if(total > hValue){
				// set new value
				hValue = total;
				// set stack position
				arPos = x;
			}
		}

		// name of the selected domino from stack
		string selName = (string)al[arPos];
		// find and select domino
		GameObject.Find(selName).GetComponent<PlayerDomino>().OnCPU();
		// start timer
		countPlace = true;
	}

	// select highest double
	void SelectHighestDouble(ArrayList al){
		int arPos = 0;
		int hValue = 0;
		
		for(int x = 0; x < al.Count; x++){
			string n = (string)al[x];
			int n1 = GameObject.Find(n).GetComponent<PlayerDomino>().no1;
			int n2 = GameObject.Find(n).GetComponent<PlayerDomino>().no2;

			if(n1 == n2){
				int total = n1 + n2;
				if(total > hValue){
					hValue = total;
					arPos = x;
				}
			}
		}
		string selName = (string)al[arPos];
		GameObject.Find(selName).GetComponent<PlayerDomino>().OnCPU();
		countPlace = true;
	}

	// play move cpu selected
	void PlaySelectedMove(){
		// find all created moves
		GameObject[] em = GameObject.FindGameObjectsWithTag("empty") as GameObject[];

		// select move
		int sm = Random.Range(0,em.Length);
		// play move
		em[sm].GetComponent<Empty>().OnFinger();
	}
}
