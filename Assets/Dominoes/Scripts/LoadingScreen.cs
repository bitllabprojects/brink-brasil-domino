﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingScreen : MonoBehaviour {

	public Image logo;
	float timer = 0.0f;

	// Use this for initialization
	void Start () {
		logo.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(timer < 5.0f){
			timer += 1.0f * Time.deltaTime;
			if(timer > 5.0f){
				timer = 5.0f;
			}
			logo.color = new Color(1.0f, 1.0f, 1.0f, (timer/5.0f));
		}else{
			logo.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			SceneManager.LoadScene("MainMenu");
		}
	}
}
