﻿using UnityEngine;
using System.Collections;

public class Setup : MonoBehaviour {

	public int playerNo;								// number of players
	public int dominoStartNo;					// number of starting tiles

	public int targetScore;						// target total score to win

	public int player1Score;						// player scores
	public int player2Score;						//
	public int player3Score;						//
	public int player4Score;						//

	public int lastRoundWinner;				// player that won last round

	public string direction;						// play direction
	public string gameType;					// game type
	public string f2p;									// first to play
	

	//
	void Awake(){
		DontDestroyOnLoad(gameObject);		//
	}

	// reset game setup
	public void ResetSetup(){
		player1Score = 0;
		player2Score = 0;
		player3Score = 0;
		player4Score = 0;

		lastRoundWinner = 0;
	}
}
