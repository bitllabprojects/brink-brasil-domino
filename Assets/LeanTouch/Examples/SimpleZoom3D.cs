using UnityEngine;

// This script will zoom the main camera based on finger gestures
public class SimpleZoom3D : MonoBehaviour
{
    [Header("2D - Camera Orthografica")]
    public float maxZoom = 0.5f;
    public float minZoom = 6.0f;
    [Header("3D - Camera Perspectiva")]
	public float Minimum = 10.0f;
	public float Maximum = 60.0f;
	
	protected virtual void LateUpdate()
	{
		// Does the main camera exist?
		if (Camera.main != null)
		{
			// Make sure the pinch scale is valid
			if (Lean.LeanTouch.PinchScale > 0.0f)
			{
				// Store the old FOV in a temp variable
				var fieldOfView = Camera.main.fieldOfView;
                var orthoSize = Camera.main.orthographicSize;

				// Scale the FOV based on the pinch scale
				fieldOfView /= Lean.LeanTouch.PinchScale;
                orthoSize /= Lean.LeanTouch.PinchScale;
				
				// Clamp the FOV to out min/max values
                fieldOfView = Mathf.Clamp(fieldOfView, Minimum, Maximum);
                orthoSize = Mathf.Clamp(orthoSize, minZoom, maxZoom);

				// Set the new FOV
                Camera.main.fieldOfView = fieldOfView;
                Camera.main.orthographicSize = orthoSize;
			}
		}
	}
}